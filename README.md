[![pipeline status](/../badges/main/pipeline.svg)](../../pipelines)

* [Latest PDF online](/../-/jobs/artifacts/main/file/thesis.pdf?job=build)
    * [Build Log](/../-/jobs/artifacts/main/file/build_thesis.log?job=build)
* [diff to last tag](/../-/jobs/artifacts/main/file/difftag_thesis.pdf?job=diff)
* [diff to last commit](/../-/jobs/artifacts/main/file/diffcommit_thesis.pdf?job=diff)


Helpful documents
* [(German) manual for the TU LaTeX template](http://mirrors.ctan.org/macros/latex/contrib/tudscr/doc/tudscr.pdf)
* [Ein Anwenderleitfaden für das Erstellen einer wissenschaftlichen Abhandlung](http://mirrors.ctan.org/macros/latex/contrib/tudscr/doc/tutorials/treatise.pdf)
