\chapter{Konzept zur Realisierung Multi-Roboter Arbeitsräume}\label{ch:concept}
Wie im vorherigen Kapitel beschrieben, charakterisieren wissenschaftliche Arbeiten die Analyse des Arbeitsraums einzelner Roboter, sowie die Kenntnis bezüglich auszuführender Aufgaben als notwendige Voraussetzungen, um anhand dieser Informationen sukzessiv die optimale Position des Roboters zu bestimmen. Dieses Kapitel dokumentiert die konzeptuellen Entscheidungen bezüglich gewählten Verfahren dieser Bachelorarbeit, basierend auf den zuvor beschriebenen Methodiken zur Arbeitsraumanalyse, dessen Invertierung und anschließender Auswertung. Das Konzept zur Definition spezifischer Aufgaben, sowie deren intuitive Generierung und Darstellung mittels grafischer Programme ist ebenfalls dokumentiert und vervollständigt somit den Algorithmus zur Kalkulation valider Roboterbasen. Die grundlegende Struktur persistierter Daten im YAML Format sind ebenfalls Bestandteil des Kapitels. Anhand dieser Realisierung erfolgt eine Erweiterung der Erkenntnisse vorangegangener wissenschaftlicher Arbeiten um die Anwendung auf mehrere robotische Systeme sowie der Konkretisierung spezifischer Aufgaben. Diese Erweiterung ermöglicht die automatisiert Konstruktion kollaborative Multi-Roboter Arbeitsräume.


\section{Arbeitsraumanalyse robotischer Systeme}
\label{sec:Aanal}
Das Generieren valider Transformationen $^{0}T_{Endeff}$, welche Resultat der Vereinigung aus $OR_{total}$ und $P_{total}$ sind, ist Bestandteil der Arbeitsraumanalyse. Dabei ist den Formulierungen zur Generierung aller Orientierungen $OR_{total}$ zu entnehmen, dass diese auf den Polarkoordinaten $^{0}s \in S^{2}$ einer Sphäre basieren, welche durch ihren Ursprung und Radius definiert ist. Der implementiere Algorithmus zur Ermittlung aller Transformationen $^{0}T_{Endeff}$ kombiniert diese Mengen, indem jeder Vektor $^{0}p \in P_{total}$ den Ursprung einer Sphäre darstellt, und demzufolge Ausgangspunkt der Kalkulation seiner Orientierungen ist. 

\subsection{Erfassung der Orientierungen und Positionen}
Basierend auf der Arbeit von Zacharias et al. \cite{Zacharias2007}, welche die Erfassung der Menge $P_{total}$ anhand von Zufallsverfahren als unzureichend bezüglich der Repräsentation des Umfelds robotischer Systeme klassifiziert, wird in der Implementierung stattdessen die kubische Diskretisierung angewandt. Demzufolge unterliegt die Menge $P_{total}$ den Größen $q, a \in \R_{+}$, welche das Intervall des entstehenden Würfels beziehungsweise den Abstand zwischen den Vektoren $^{0}p \in P_{total}$ definieren.\par
Die Erfassung aller Orientierungen $OR_{total}$ basiert ebenfalls nicht auf Pseudozufall, da die anschließenden kinematischen Berechnungen den zeitaufwändigsten Faktor der Arbeitsraumanalyse darstellen, folglich ist eine Implementierung, dessen Resultat von der kumulativen Betrachtung einer enormen Anzahl zufällig generierter sphärischer Koordinaten abhängt, gleichermaßen unzulänglich. Dies impliziert den Einsatz der sphärischen Diskretisierungen anhand gleichmäßiger beziehungsweise ungleichmäßiger Verteilungen. 
Durch die Wahl des festen Abstands $a$ bezüglich der kubischen Diskretisierung und Definition der Vektoren $^{0}p \in P_{total}$ als Zentrum einer Sphäre, wird der Radius $r \in \R_{+}$ der Sphäre nach \autoref{eq:23} kalkuliert. 

\begin{equation}
\label{eq:23}
r = \frac {a}{2}
\end{equation}

Zusätzlich empfiehlt sich die vollständige Auslagerung der Berechnungen auf performante Rechner, um diese mit mehreren Rechenkernen zu operieren. Beispielsweise empfiehlt sich für diese Arbeit das Hochleistungsrechenzentrum der TU-Dresden. \par
\autoref{lst:1} zeigt die Struktur der YAML-Datei, welche eine reachability map schematisch repräsentiert. 
\begin{lstlisting}[language=JSON,firstnumber=1,float, caption={Struktur der reachability map},captionpos=t, label={lst:1}]
Referenzsystem: str % "Emika Franka 'Panda'"
Map_Name: str % : "RM_\$(Intervall)_\$(Aufloesung)"
Aufloesung: float
Intervall: float
Threads: int
Zeit: float
OR_total: int
Endeffektor_Pose: [str]  % "x y z q_x q_y q_z q_w"
% - "-1.250 -1.250 -1.250 0.000 0.831 0.000 0.556"
% - "-1.250 -1.250 -1.250 -0.831 -0.000 0.556 -0.000"
IK_Loesung: [bool]
% - false
% - false
\end{lstlisting}

Die Felder \texttt{Aufloesung} und \texttt{Intervall} entstammen den Parametern der kubischen Diskretisierung und definieren den Namen der Datei, sowie das Feld \texttt{Map\_Name}. \texttt{Endeffektor\_Pose} enthält eine Sequenz aus String-Repräsentation aller Endeffektor-Posen. Diese bestehen aus Position und Orientierung, wobei die Orientierung als Quaternion notiert ist, was dem Präfix q\_ zu entnehmen ist. \texttt{Zeit} enthält die Zeitspanne, in der alle kinematischen Berechnungen erfolgen, in Sekunden. Das Ergebnis der kinematischen Funktion aus \autoref{eq:7} als Informationen bezüglich der Erreichbarkeit der Pose ist als Wahrheitswert in der Sequenz des Feldes \texttt{IK\_Loesung} dokumentiert. Der Name des durchführenden robotischen Systems, die Kardinalität von $OR_{total}$ und die Anzahl der angewandten Rechenkernen werden in den Feldern \texttt{Referenzsystem}, \texttt{OR\_total} beziehungsweise \texttt{Threads} erfasst.


\subsection{Komplexität}
Derart zeitaufwändigen Verfahren erfordern die Betrachtung der Komplexität, um anhand dessen die Anzahl aller auszuführenden Operationen und somit dessen maximalen Zeitaufwand  abzuschätzen. Die \autoref{eq:24} formulierte Einordnung der zeitlichen Komplexität erfolgt durch die Betrachtung der Kardinalitäten der jeweiligen Mengen.

\begin{equation}
\label{eq:24}
\begin{split}
& \mathcal{O}(\vert P_{total} \vert \times \vert OR_{total} \vert)  \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert OR_{total} \vert)  \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert S \vert \times \vert Roll \vert) 
\end{split}
\end{equation}

Davon ist abzuleiten, dass die Wahl der Faktoren $q, a \in \R_{+}$, sowie die Menge sphärischer Vektoren $S^{2}$ und der zusätzlich betrachteten Roll Winkel, welche Grundlegende Parameter der kubischen Diskretisierung beziehungsweise sphärischen Diskretisierung darstellen, die Komplexität kubisch oder linear beeinflussen. Basierend auf den folgenden Überlegungen und daraus resultierenden Vereinfachungen, kann die Auswirkung der sphärischen Komponente auf die Komplexität dezimiert werden. 

\subsubsection{Vereinfachung des Roll Segments}
Um die Komplexität zu beschränken, wird beruhend auf der Bauart eines Roboterarms die Annahme getroffen, dass der Endeffektor zu jeder Zeit eine vollständige Drehung um seine longitudinal Achse vollziehen kann. Dies reduziert die Kardinalität $\vert OR_{total} \vert$ um das Roll Segment und stellt ein Ersparnis hinsichtlich einer Dimension an Berechnungen während der Analyse dar. Daraus resultiert die Menge $OR_{total}^{/R}$, dessen Kardinalität \autoref{eq:25} zeigt.

\begin{equation}
\begin{split}
\label{eq:25}
& \vert ^{0}OR_{total} \vert = \vert S^{2} \vert \times \vert Roll \vert \\
& \vert ^{0}OR_{total}^{/R} \vert = \vert S^{2} \vert 
\end{split}
\end{equation} 


\subsubsection{Vereinfachung der sphärischen Vektoren}
Eine weitere Vereinfachung stellt die Annahme dar, dass Objekte nicht von unten gegriffen werden können beziehungsweise dieses spezifische vorgehen nicht Teil der zu bewältigenden Aufgabe ist. Dies stellt eine Halbierung bezüglich der sphärischen Vektoren dar, indem für eine Sphäre $S^{2}$ alle Vektoren $^{0}s \in S^{2}$, deren Element $^{0}sz \in ^{0}s$ kleiner als das dazugehörige $^{0}pz \in ^{0}p $ Element des Zentrums $^{0}p$ bezüglich der assoziierten Sphäre ist. \autoref{eq:26} formuliert die resultierende Menge $S_{/2}^{2}$ und dessen Kardinalität. 

\begin{equation}
\label{eq:26}
\begin{split}
& S_{/2}^{2} = \{ ^{0}sz > ^{0}pz  | ^{0}sz \in ^{0}s, ^{0}pz \in ^{0}p \} \\
& \vert S_{/2} \vert = \frac{\vert S^{2} \vert}{2}
\end{split}
\end{equation}

\subsubsection{Vereinfachte Komplexität}
Aus den Vereinfachungen bezüglich verschiedener Aspekte der Menge $OR_{total}$ und dessen zugrundeliegender sphärischer Diskretisierung, resultiert durch deren Anwendung in \autoref{eq:27} die zeitliche Komplexität $\mathcal{O}((\frac{2 \times q}{a})^{3} \times \frac{\vert S^{2} \vert}{2})$.

\begin{equation}
\label{eq:27}
\begin{split}
& \mathcal{O}(\vert P_{total} \vert \times \vert OR_{total} \vert) \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert OR_{total} \vert)   \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert OR_{total}^{/R} \vert)   \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert S^{2} \vert)  \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \vert S_{/2}^{2} \vert)  \\
& \mathcal{O}((\frac{2 \times q}{a})^{3} \times \frac{\vert S^{2} \vert}{2})
\end{split}
\end{equation}

\subsection{Verfahren der sphärischen Diskretisierung}
Aus vorangegangenen Kapiteln ist bekannt, dass die sphärische Diskretisierung durch Abtastung der Winkelintervalle oder der Verteilung von $n \in \N $ Vektoren $^{0}s$ auf einer Sphäre, erfolgt. Das Abtasten der Winkelintervalle basierend auf einer festen Auflösung generiert ein ungleichmäßiges sphärisches Gitter, wodurch die Vektoren $^{0}s$ nicht äquidistant verteilt sind. Zusätzlich ist aus \autoref{fig:7} ersichtlich, dass die Distanz der Vektoren an den Polen marginal ist. Diese Beobachtungen implizieren, dass die resultierende Menge $OR_{total}$ durch dessen hohes Maß an ähnlichen und uneinheitlichen Orientierungen, für die Bewertung der Erreichbarkeit nicht geeignet ist. Demnach ist die äquidistante Verteilung der Vektoren $^{0}s$ für eine qualitative Repräsentation der Erreichbarkeit eines Vektors $^{0}v_{Endeff}$, sowie der Dezimierung des zeitlichen Aufwands aller kinematischen Berechnungen, maßgeblich ist.

\begin{figure}
\centering
\includegraphics[width = 6cm, height = 6.5cm]{images/sphere.png}
\caption[Sphärische ungleichmäßige Diskretisierung]{Gitter aus nicht äquidistanten Vektoren einer sphärischen Diskretisierung. Die Distanz zwischen Vektoren an den Polen ist vernachlässigbar gering.}
\label{fig:7}
\end{figure}

\subsubsection{Gleichmäßige Verteilung}
Methoden wie die Fibonacci- oder Spiral- Verteilungen bieten minimale Abweichungen, empfehlen sich daher für die Orientierungsgenerierung und bilden eine im Kontext dieser wissenschaftlichen Arbeit angemessene Repräsentation der Erreichbarkeit des Vektors $^{0}p_{Endeff}$. Sie gehören neben der ungleichmäßigen Diskretisierung zu den von Mikhal et al. angewendeten Methoden zur Arbeitsraumanalyse eines robotischen Systems \cite{Makhal2018}. Die Deserno \cite{Deserno2004} illustrierte äquidistante sphärische Diskretisierung einen wurde bisher nicht zum Zweck der Roboterplatzierung genutzt wurde. Der dokumentierte Algorithmus wurde für diese wissenschaftliche Arbeit übernommen.

\section{Invertierte Arbeitsräume}
\label{sec:IA}
Ziel dieses Abschnitts ist die Präzisierung des grundlegenden Ansatzes, dessen sich hinsichtlich der Berechnung optimaler Positionen bezüglich der Platzierung robotischer Systeme bedient wird. Dies beinhaltet die Anwendung der in den Grundlagen aufgeführten kinematischen Sachverhalte, sowie der Voraussetzung einer Pose $t \in A$ des zu operierenden Objekts, wobei $A \subset SE(3)$ die Menge aller Aufgabenposen relativ zum $Frame_{0}$ ist.  

Eine persistierte reachability map beinhaltet eine Liste aller Vektoren $v \in P_{total}$ des kubisch diskretisierten Roboterumfeldes mit dessen zugehörigen Orientierungen $R \in OR_{total}$, zuzüglich des Ergebnisses der Funktion $f_{kin}$ aus \autoref{eq:7}. Jede positiv terminierte kinematische Berechnung klassifiziert die operierte Transformation als valide Endeffektor Pose. Für jede dieser validen Transformationen gilt nach \autoref{eq:6}, dass eine kinematische Kette jener Form existiert.

Die Roboterbasis kann der Pose des ersten Festkörpers in Form der Transformation $^{0}T_{Base}$ eines robotischen Systems entnommen werden, wie die Umformungen in \autoref{eq:28} sukzessiv zeigen. Zum besseren Verständnis erfolgt diese Berechnung für eine kinematische Kette aus $n = 8$ Gliedern. Dabei ist in diesem Beispiel die Transformation von $Frame_{0}$ zu $Frame_{1} = Frame_{Base}$ die Identität 1, da der Roboter keine Erhöhung durch einen Standfuß aufweist, sondern seine Basis $^{0}T_{Base}$ auf dem fixen $Frame_{0}$ des kinematischen Baumes liegt. Sollte dem nicht so sein, ist eine zusätzliche Berechnung erforderlich.

\begin{equation}
\begin{aligned}
^{0}T_{Base} &= t \times ^{0}T_{Endeff} \\
 &= t \times (\prescript{0}{}{T}_{1} \times \prescript{1}{}{T}_{2} \times \prescript{2}{}{T}_{3} \times \prescript{3}{}{T}_{4} \times \prescript{4}{}{T}_{5} \times \prescript{5}{}{T}_{6} \times \prescript{6}{}{T}_{7} \times \prescript{7}{}{T}_{Endeff}) &\text{\autoref{eq:6}} \\
 &= \prescript{0}{}{T}_{Endeff} \times (\prescript{0}{}{T}_{1} \times \prescript{1}{}{T}_{2} \times \prescript{2}{}{T}_{3} \times \prescript{3}{}{T}_{4} \times \prescript{4}{}{T}_{5} \times \prescript{5}{}{T}_{6} \times \prescript{6}{}{T}_{7} \times \prescript{7}{}{T}_{Endeff})^{-1} &\text{t = $\prescript{0}{}{T}_{Endeff}$}\\
 &= \prescript{0}{}{T}_{Endeff} \times (\prescript{Endeff}{}{T}_{7} \times \prescript{7}{}{T}_{6} \times \prescript{6}{}{T}_{5} \times \prescript{5}{}{T}_{4} \times \prescript{4}{}{T}_{3} \times \prescript{3}{}{T}_{2} \times \prescript{2}{}{T}_{1} \times \prescript{1}{}{T}_{0})  &\text{\autoref{eq:SE}}\\
 &= \prescript{0}{}{T}_{Endeff} \times (\prescript{Endeff}{}{T}_{7} \times \prescript{7}{}{T}_{6} \times \prescript{6}{}{T}_{5} \times \prescript{5}{}{T}_{4} \times \prescript{4}{}{T}_{3} \times \prescript{3}{}{T}_{2} \times \prescript{2}{}{T}_{Base} \times 1) &\text{\autoref{sec:IA}}\\
 &= \prescript{0}{}{T}_{Endeff} \times (\prescript{Endeff}{}{T}_{7} \times \prescript{7}{}{T}_{6} \times \prescript{6}{}{T}_{5} \times \prescript{5}{}{T}_{4} \times \prescript{4}{}{T}_{3} \times \prescript{3}{}{T}_{2} \times \prescript{2}{}{T}_{Base}) \\
 &= \prescript{0}{}{T}_{Endeff} \times ^{Endeff}T_{Base} &\text{\autoref{eq:5}} \\
 &= ^{0}T_{Base} &\text{\autoref{eq:5}}
\end{aligned}
\label{eq:28}
\end{equation}
 
Dazu wird die Menge $IRM$ aus \autoref{eq:19} in eine YAML Datei persistiert, dessen Struktur der \autoref{lst:2} schematisch zeigt, wobei die Felder \texttt{Basis\_Pose} und \texttt{Metrik} aus der zugrundeliegenden reachability map kalkuliert werden. Die Felder \texttt{Referenzsystem}, \texttt{Aufloesung} und \texttt{Intervall} werden aus der reachability map übernommen, und bilden analog den Namen \texttt{Map\_Name} der Datei. \texttt{Zeit} dokumentiert die Zeit zur Kalkulation aller Posen $^{Endeff}T_{0}$. Das Ergebnis dieses Algorithmus kann bei Formeln der Form \autoref{eq:20} angewendet werden, um die Roboter Positionierung hinsichtlich der Definition einer spezifischen Aufgabe zu errechnen.

\begin{lstlisting}[language=JSON,firstnumber=1, caption={ Struktur der inverse reachability map},captionpos=t, float, label={lst:2}]
Referenzsystem: str % $(Referenzsystem) aus reachability map
Map_Name: str % "IRM_\$(Intervall)_\$(Aufloesung)"
Aufloesung: float % $(Aufloesung) aus reachability map
Intervall: float % $(Intervall) aus reachability map
Zeit: float 
Basis_Pose: [str] % ["x y z q_x q_y q_z q_w"]
% - "0.518 -0.250 -0.597 0.831 0.000 -0.555 -0.000"
% - "0.242 -0.775 0.170 0.849 -0.490 -0.169 0.098"
Metrik: [float]
% - 0.375
% - 0.375
\end{lstlisting}

\section{Definition spezifischer Aufgaben}
Das Konzept zur Planung und Erstellung spezifischer Aufgaben, sowie deren Realisierung innerhalb einer Szene und Visualisierung in einer geeigneten graphischen Umgebung wie beispielsweise RViz, ist Inhalt dieses Abschnittes. Dabei besteht eine Szene aus Kollisionsobjekten, welche Hindernisse oder Primitive definieren, die das robotische System umgehen beziehungsweise mit denen es Interagieren muss. Die Planung und Ausführung einer Aufgaben erfolgt über spezifische Nachrichten der move\_group Schnittstelle, welche dem Roboter die auszuführende Operation detailliert kommunizieren, Primitive in der Szene spezifizieren und Posen festgelegt, die vor und nach der Handlung angenommen werden sollen.
Aktuell bietet MoveIt die Möglichkeit, beliebig Kollisionsobjekte in der Szene zu generieren und persistieren, aber keinen Algorithmus zur Aufgabenbeschreibung, welcher die Reihenfolge konkretisiert, in der die Kommunikation mit den robotischen Systemen stattfindet. Diese fehlende Funktion wird mittels eines Algorithmus ergänzt, welcher die Interaktionen, wie beispielsweise die Translation und Rotation eines Kollisionsobjektes, durch den Nutzer und der grafischen Bedienoberfläche realisiert und anhand zusätzlicher Optionen die Dokumentation von Aufgabenbeschreibungen ermöglicht. Dabei empfehlen sich Interaktive Marker durch ihren Funktionsumfang zur Repräsentation der Kollisionsobjekte, und bieten weitere Optionen zur Aufgabenbeschreibung in ihren Menüs.
Eine valide Aufgabenbeschreibung kann ebenfalls in YAML Dateien persistiert werden, dessen Struktur der \autoref{lst:3} veranschaulicht. Der Nutzer hat die Möglichkeit, einen Titel der Aufgabe zu definieren, welche im Feld \texttt{Name der Aufgabe} erfasst wird. Das Feld \texttt{Aufgabenbeschreibung} ist eine Sequenz beziehungsweise Liste aus Objekten, dessen Inhalt eine weitere Sequenz aus Aufgabenpositionen in Form einer Kette ist, welche das jeweilige robotische System bearbeitet. Die Orientierung der Aufgabe ist nicht auf das zugrundeliegende Objekt bezogen, sondern dokumentiert die Rotation des Endeffektors um das Objekt beispielsweise zu greifen oder abzustellen. Demnach werden diese Rotationen pro Aufgabenposition nach den Algorithmen in \autoref{sec:ORS} generiert.

\begin{lstlisting}[language=JSON,firstnumber=1, float, caption={Struktur einer Aufgabenbeschreibung},captionpos=t, label={lst:3}]
Name_der_Aufgabe: str
Aufgabenbeschreibung: 
	objekt : [str] % "x y z"
\end{lstlisting}

\subsection{Operationsketten}
Aufgaben, wie das sequentielle Greifen und Abstellen eines Objektes gehören zur Aufgabengruppe \emph{Pick and Place}, welche in dieser Arbeit hauptsächlich untersucht werden. Diese Aufgaben werden mittels der Positionen konkretisiert, an denen eine Operation erfolgt. Diese sukzessiv zu operierende Menge an Positionen bildet eine Operationskette $OK$. Die Menge aller Aufgaben, die Operationsketten abbilden, unterliegen der Partitionierung von $OK$. So kann eine Kette bestehend aus $k \in \N_{>1}$ Partitionen von $k$ Robotern unabhängig voneinander oder kooperativ operiert werden. \autoref{fig:OK} illustriert eine solche Operationskette mit $n = 5$ Kettengliedern. Das jeweils erste Glied einer Partition ist grün markiert, während Folgeglieder blau markiert sind. Jedes Glied ist aufsteigend nummeriert, wodurch die Reihenfolge impliziert wird, in der ein Objekt beziehungsweise gegriffen oder abgestellt werden soll. Die Färbung der Pfeile visualisiert den Roboter, der dieser Partition zugewiesen ist und diese ausführt. 

\begin{figure}[h!]
\centering
\begin{tikzpicture} [ >=stealth']
		\node[rectangle, draw=green!60, fill=green!5,  thick, minimum size=3em, align=center] (a) {0};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aa) [above right= of a]{1};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaa) [below right= of aa]{2};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaaa) [above right= of aaa]{3};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaaaa) [below right= of aaaa]{4};
	\draw[->] (a) -- (aa);
	\draw[->] (aa) -- (aaa);
	\draw[->] (aaa) -- (aaaa);
	\draw[->] (aaaa) -- (aaaaa);
\end{tikzpicture}
\caption{Zwei dimensionale Darstellung einer Operationskette}
\label{fig:OK}
\end{figure}

\subsubsection{Gespaltene Operationskette}
Gespaltene Operationsketten partitionieren $OK$ in $i \in \N_{>0}$ disjunkte Teilketten $TK  \subset OK $, welche $i$ Roboter isoliert voneinander bearbeiten. 
Diese Aufgaben sind implizit durch die enthaltenen Teilketten im Positionsfeld der Aufgabenbeschreibung gegeben, indem alle enthaltenen Positionen disjunkt sind, das heißt es gibt keine Sequenz eines Objektes, die sich als Position eines anderen Objektes wiederholen. \autoref{fig:hallo} zeigt eine gespaltene Operationskette mit zwei Teilkette. Die grünen Kettenglieder markieren jeweils den Start, an dem sich ein Kollisionsobjekt befindet. Die Anordnung und Färbung der Pfeile sowie die Nummerierung impliziert, dass zwei robotische Systeme diese Aufgabe getrennt voneinander ausführen.  

\begin{figure}[h!]
\centering
\begin{tikzpicture} [ >=stealth']
		\node[rectangle, draw=green!60, fill=green!5,  thick, minimum size=3em, align=center] (a) {0};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aa) [above right= of a]{1};
		\node[rectangle, draw=green!60, fill=green!5,  thick, minimum size=3em, align=center] (aaa) [below right= of aa]{2};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaaa) [above right= of aaa]{3};
	\draw[->, draw=cyan!60] (a) -- (aa);
	\draw[->, draw=magenta!60] (aaa) -- (aaaa);
\end{tikzpicture}
\caption{Zwei dimensionale Darstellung einer Operationskette bestehend aus disjunkten Teilketten}
\label{fig:hallo}
\end{figure}

\subsubsection{Kooperative Operationskette}
Kooperative Ketten partitionieren $K$ in $i$ Teilketten $TK_{0}... TK_{i-1}  \subset OK $, deren Schnittmenge das jeweils letzte und erste Glied von $TK_{j}$ beziehungsweise $TK_{j+1}$, für $0 \leq j < i$, ist. Diese Einteilung impliziert, dass die Position innerhalb der Abstellnachricht des Vorgängerroboters ebenfalls Inhalt der Greifnachricht des Roboters $i$ ist. Diese Logik ist implizit aus der Sequenz eines Objekts im \autoref{lst:3} entnehmbar, indem eine Position wiederholt vorkommt. \autoref{fig:koop} Visualisiert das Kettenglied, welches zugleich End-, als auch Anfangsglied zweier Teilketten ist, farblich rot. Die Position ist demzufolge wiederholt, aufeinander folgend in der Aufgabenbeschreibung notiert.

\begin{figure}[h!]
\centering
\begin{tikzpicture} [ >=stealth']
		\node[rectangle, draw=green!60, fill=green!5,  thick, minimum size=3em, align=center] (a) {0};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aa) [above right= of a]{1};
		\node[rectangle, draw=red!60, fill=red!5,  thick, minimum size=3em, align=center] (aaa) [below right= of aa]{2};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaaa) [above right= of aaa]{3};
		\node[rectangle, draw=blue!60, fill=blue!5,  thick, minimum size=3em, align=center] (aaaaa) [below right= of aaaa]{4};
	\draw[->, draw=cyan!60] (a) -- (aa);
	\draw[->, draw=cyan!60] (aa) -- (aaa);
	\draw[->, draw=magenta!60] (aaa) -- (aaaa);
	\draw[->, draw=magenta!60] (aaaa) -- (aaaaa);
\end{tikzpicture}
\caption{Zwei dimensionale Darstellung einer kooperativen Operationskette}
\label{fig:koop}
\end{figure}

\section{Positionsanalyse}
Aus den persistierten Informationen der Arbeitsraumanalyse, dem Ansatz der invertierten Arbeitsräume und dessen Vereinigung mit den Aufgabenbeschreibungen resultiert die Menge der Basispositionen $P_{Base}$ kombiniert mit der Metrik $D_{Reach}$ aus \autoref{eq:18} für jeden Vektor $^{0}p_{Base} \in ^{0}P_{Base}$.
Der in \autoref{sec:positioning} beschriebene Algorithmus zur Ermittlung der optimalen Position sieht vor, anhand der Voxelisierung einen Index für jeden Voxel zu kalkulieren und daraus Rückschlüsse bezüglich der Positionierung robotischer Systeme zu ziehen. Nachteile dieses Vorgangs sind:

\begin{enumerate}
  \item Intransparenz der Menge $P_{Base}$ bezüglich der Vektor $^{0}p_{Base}$ \label{enumi:Intransparenz}
  \item Metrik $D_{Voxel}$ ist nicht aussagekräftig für getrennte Aufgabenbeschreibungen\label{enumi:Metrik}
\end{enumerate}

Der in \autoref{enumi:Intransparenz} beschriebene Aspekt bezieht sich auf die Vektoren $^{0}p_{Base} \in P_{Base}$, dessen Herkunft hinsichtlich der Aufgabenbeschreibung nicht ersichtlich ist. Daraus ist abzuleiten, dass die Partitionierung der Operationskette keinen Einfluss auf den Algorithmus hat, wodurch dessen Eignung für die Anwendung bezüglich mehrerer robotischer Systeme entfällt. Daraus ergibt sich implizit \autoref{enumi:Metrik}, denn die Metrik $D_{Voxel}$ aus \autoref{eq:22} bewertet einen Voxel, ohne dessen Inhalt bezüglich der Aufgabenbeschreibung zu gewichten. So werden beispielsweise Voxel mit hoher Kardinalität besser für die Aufgabe gewertet, obwohl dessen Inhalt bezüglich spezifischer Aufgabenpositionen mangelhaft ist. Basierend auf diesen Beobachtungen ist eine abstrahierende Betrachtung hinsichtlich der Kettenstruktur innerhalb der Aufgabenbeschreibung zielführender.

\subsection{differenzierte Voxelisierung}
\label{sec:diffVoxel}
Die beschriebenen Nachteile in \autoref{enumi:Intransparenz} und \autoref{enumi:Metrik} zeigen, dass der Algorithmus aus \autoref{sec:positioning} nicht auf mehrere robotische Systeme anwendbar ist. Diese Erkenntnis erfordert die Implementierung eines optimierten Ansatzes der Voxelisierung, indem die Operationsketten bezüglich einer Aufgabenbeschreibung in den Prozess integriert werden. Während der differenzierten Voxelisierung wird jede Teilkette $TK \subset OK$ einzeln in  der \autoref{eq:28} betrachtet. 
Diese Betrachtung hat folgende Vorteile:

\begin{enumerate}
  \item Filterung der Voxel nach Teilkette \label{enumi:filter}
  \item Kettenglieder anderer Teilketten haben keinen Einfluss auf die Bewertung des Voxels \label{enumi:einfluss}
\end{enumerate}

Durch die Dokumentation der zugrundeliegenden Multiplikation jedes Kettenglieds einer Teilkette $^{0}k \in TK$, dessen Resultat eine Basisposition $^{0}p_{Base} \in P_{Base}$ ist, können durch eine Iteration ermittelt werden, welches Voxel Basisposition aller Kettenglieder einer Teilkette enthält. Dieser implementierte Filter gleicht der Definition einer Schnittmenge aus allen Basisposition pro Kettenglied einer Teilkette. Die Division des arithmetischen Mittels aus \autoref{eq:21}, angewendet auf diese gefilterten Voxel, mit der Kardinalität der Teilkette ergeben die Bewertung eines Voxels bezüglich der Teilkette. Diese differenzierte Betrachtung verhindert, dass Kettenglieder anderer Teilketten den Wert eines Voxels beeinflussen.

\section{Platzierung der robotischen Systeme}
\label{sec:ground}
Die Platzierung der robotischen Systeme erfolgt innerhalb der gewählten Domäne. Diese ist beispielsweise die Oberfläche eines oder mehrerer Tische, wie es im Ceti der Fall ist. Diese Oberfläche wird durch einen Bereich auf der XY Ebene imitiert, dessen Dimensionen angepasst werden können. Alle Voxel der Schnittmenge einer Teilkette werden dahingehend inspiziert, ob deren Basispositionen die gewählte Domäne schneiden. Anschließend erfolgt die Wahl des Voxels mit der maximalen Bewertung und die Basisposition wird abschließend festgelegt. Dieser Vorgang definiert die Zuteilung robotischer Systeme auf die Teilketten einer Aufgabenbeschreibung und wird als YAML-Datei persistiert. \autoref{lst:Goal} zeigt schematisch die Verteilung der robotischen Syteme auf die Kollisionsobjekte. Die \texttt{Roboter\_Anzahl} entspricht der Anzahl aller Teilketten und spezifiziert die Anzahl der robotischen Systeme. Die während der differenzierten Voxelisierung vergangene Zeit wird im entsprechenden Feld erfasst. Die Basispositionen aller Roboter werden im \texttt{Basispositionen} Feld dokumentiert. Alle zusätzlichen Felder sind aus der zugrundeliegenden inverse reachability map, beziehungsweise der Aufgabenbeschreibung portiert.

\begin{lstlisting}[language=JSON,firstnumber=1, caption={ Struktur der Positionsanalyse},captionpos=t, label={lst:Goal}]
Referenzsystem: str % "$(Referenzsystem)" aus inverse reachability map
Name_der_Aufgabe: str % "$(Name_der_Aufgabe)" aus Aufgabenbeschreibung
Roboter_Anzahl: int 
Zeit: float
Move_Groups: 
	objekt:
		panda_arm: [str] % "$(Aufgabenbeschreibung)" aufgetilt auf die Roboterarme
Basispositionen:
	panda_arm: str % "x y z"
\end{lstlisting}