\chapter{Fallbeispiel}\label{ch:caseStudy}
Inhalt dieses Kapitels ist die Demonstration der Funktionsweise einzelner Elemente der Implementierung und dessen Synergie mit \emph{MoveIt!} spezifischen Komponenten, sowie der Integration des franka\_ros Pakets\footnote{\url{http://wiki.ros.org/franka_ros}} anhand eines Beispiels. Dieses Paket stellt hierbei sowohl die grafische, als auch funktionelle Beschreibung der Bestandteile des gleichnamigen robotischen Systems zur Verfügung, welches primär Objekt der Ausführung ist. 

\section{Ausgangspunkt kinematischer Operationen}
\emph{MoveIt!} bietet über die \emph{move\_group} Schnittstelle eine Möglichkeit der Kommunikation mittels spezieller Nachrichten und somit der Planung und Exekution kinematischer Operationen durch den Roboter. Dies benötigt eine \emph{SRDF} Datei, welche das robotische System konkretisiert, indem beispielsweise Kontrollelemente wie Endeffektoren als Gruppen definiert oder Adjazenzen der einzelnen Festkörper in Form einer Kollisionsmatrix erfasst werden. Wie \autoref{sec:Moveit} zu entnehmen ist, kann diese Konfigurationsdatei innerhalb eines ROS Pakets mit dem \emph{moveit setup assistant} aus einer validen Roboterbeschreibung generiert werden. Das resultierende Konfigurationspaket ist Ausgangspunkt aller operativen Aspekte des Fallbeispiels und daher sowohl für einen Roboter, als auch für mehrere robotische Systeme in der Implementierung enthalten. 

\subsection{Modifikationen in der Beschreibung robotischer Systeme}
\label{sec:Modifikationen}
Die \emph{move\_group} Schnittstelle bietet keine Möglichkeit zur Deklaration einer Roboterposition, da diese fest in der Roboterbeschreibung definiert ist, welche vom spezifischen Konfigurationspaket geladen wird. Dies impliziert die Notwendigkeit einer Modifikation, indem die geforderten Positionen in Form von Variablen sukzessiv innerhalb der Instanzen eines Konfigurationspakets bis zur letzten Instanz, der zugrundeliegenden Roboterbeschreibung, kommuniziert werden.

\section{Präparation des Fallbeispiels}
Die Kalkulation valider Positionen für robotische Systeme erfordert die roboterzentrierte Inspektion des Arbeitsraums und eine Aufgabenbeschreibung, welche für $i \in \N_{>0}$ Endsysteme mindestens $i$ Teilketten einer Operationskette aufweist. Die diesbezüglich vorzunehmende Arbeitsraumanalyse und dessen Inversion ist aufgabenunabhängig, erfolgt daher im Vorfeld und kann auf alle Aufgabenbeschreibung angewendet werden, die vom Referenzsystem der Analyse, wie beispielsweise dem \emph{Panda} Roboter der Franka Emika GmbH, operiert werden sollen. Die Aufgabenbeschreibung kann zum Ermittlungszeitpunkt vorgenommen werden oder schon persistiert vorliegen.

\subsection{Aufgabenkonstruktion}
\label{sec:Konstruktion}
Die Aufgabenbeschreibung in Form einer Operationskette erfolgt über interaktive Marker, die jeweils die Abstell- beziehungsweise Greifposition eines Primitives darstellen. Jede dieser Positionen erfordert ein zusätzliches Kollisionsobjekt als Auflagefläche \emph{Support\_Surface}, welche dem robotischen System über spezifische Nachrichten kommuniziert wird und ohne die eine Exekution nicht erfolgt. Dieser redundante Aufwand wird im Algorithmus berücksichtigt und ist implizit durch die Position und Dimension des Primitives definiert. \autoref{fig:OPkette} illustriert eine Szene aus Kollisionsobjekten anhand des Beispiels in \autoref{fig:OK}. Die Aufgabe in Form einer Operationskette wird in \autoref{fig:OK1} mittels interaktiver Marker modelliert. \autoref{fig:OK2} zeigt die vollständig generierte Szene bestehend aus dem Primitiv und den Auflageflächen, auf denen es während des Vorgangs beispielsweise abgestellt oder von denen es gegriffen werden kann. 

\begin{figure}[h!]
\ffigbox[\FBwidth]%
  {\begin{subfloatrow}[2]
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.47\textwidth, height = 6cm]{images/OK.png}}}%
      {\caption{Operationskette mit interaktiven Markern}\label{fig:OK1}}%
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.47\textwidth, height = 6cm]{images/PS.png}}}%
      {\caption{Generierte Primitive und Auflageflächen als Kollisionsobjekte} \label{fig:OK2}}
  \end{subfloatrow}}
  {\caption[Generierung einer Szene]{Aufbau einer Szene mit Primitiven und Auflageflächen aus einer Aufgabenbeschreibung.}
\label{fig:OPkette}}%
\end{figure}

Ausgangspunkt einer Operationskette und dessen Teilketten ist das Startglied, welches durch seine Identität $id=0$ und der Farbe gekennzeichnet ist. Es ist stets möglich, ein weiteres Glied zu generieren, welches farblich und schriftlich gekennzeichnet ist. Dieser Vorgang wird in \autoref{fig:add} visualisiert.

\begin{figure}[h!]
\ffigbox[\FBwidth]%
  {\begin{subfloatrow}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/task1.png}}}%
			{\caption{Visualisierung der Option im Menü}}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/nach_join.png}}}%
			{\caption{Generierung des blauen Glieds}}
  \end{subfloatrow}}
  {\caption{Generierung eines Kettenglieds}
\label{fig:add}}%
\end{figure}

Die Schnittoption im Menü des Startknotens dupliziert diesen und ist ab einer Operationskette beziehungsweise Teilkette der Länge zwei möglich. Die Realisierung dieser Option wird in \autoref{fig:schnitt} veranschaulicht. Eine valide Aufgabenbeschreibung ist nur formulierbar und wird persistiert, wenn mindestens zwei Teilketten existieren die jeweils mindestens zwei Glieder enthalten, da dies die Notwendigkeit zweier robotischer Systeme und das Vorhandensein einer Greif- und Abstellposition impliziert. Die Wiederherstellung des Ausgangszustands ist ebenfalls durch einen zusätzlichen Menüpunkt möglich, dies eliminiert alle Glieder bis auf den Startknoten. 

\begin{figure}[h!]
\ffigbox[\FBwidth]%
  {\begin{subfloatrow}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/schnitt1.png}}}%
			{\caption{Visualisierung der Schnittoption im Menü}}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/schnitt2.png}}}%
			{\caption{Realisierung durch ein zusätzliches Startglied}}
  \end{subfloatrow}}
  {\caption{Schnitt der Operationskette in Teilketten}
\label{fig:schnitt}}%
\end{figure}

Anlog modifiziert die Kooperationsoption das jeweils letzte Glied der Operationskette beziehungsweise Teilkette und visualisiert diese Schnittmenge ebenfalls farblich, was \autoref{fig:koop} zeigt. Dies impliziert, dass eine Operationskette aus Schnitt- und Kooperationskomponenten bestehen kann, was für deren Anwendung auf $n \in \N_{>2}$ robotischen Systemen relevant ist.

\begin{figure}[h!]
\ffigbox[\FBwidth]%
  {\begin{subfloatrow}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/koop1.png}}}%
			{\caption{Visualisierung der Kooperationsoption im Menü}}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.35\textwidth, height = 6cm]{images/koop2.png}}}%
			{\caption{Modifikation des letzten Kettenglieds als Startglied einer weiteren Teilkette}}
  \end{subfloatrow}}
  {\caption[Modellierung einer kooperativen Kette]{Realisierung eines kooperativen Glieds als Schnittmenge der ersten und zweiten Teilkette}
\label{fig:koop}}%
\end{figure}

Dieser Algorithmus erzeugt Aufgabenbeschreibungen, die einem Objekt beziehungsweise Primitiv alle Positionen zuordnet, an denen es vom robotischen System beispielsweise abgestellt oder gegriffen werden soll. Eine Aufgabenbeschreibung analog zur in \autoref{fig:hallo} modellierten Aufgabe ist in \autoref{lst:schnitt} beispielhaft dokumentiert. Der Schnitt ist durch das zweite Objekt dargestellt, welches von einem zusätzlichen robotischem System bearbeitet werden soll.  

\begin{lstlisting}[language=JSON,firstnumber=1, float, caption={Aufgabenbeschreibung einer geteilten Aufgabe},captionpos=t, label={lst:schnitt}]
Name_der_Aufgabe: Schnitt
Aufgabenbeschreibung: 
	objekt0:
		- 0.000 -0.690 0.150
		- -0.550 -0.094 0.165
	objekt1:
		- 0.073 0.479 0.176
		- -0.625 0.957 0.173
\end{lstlisting}

Analog erfordert eine kooperative Aufgabenbeschreibung keine zusätzlichen Primitive, da diese beispielsweise an einer Stelle zwischen den robotischen Systemen übergeben werden. Diese Übergabeposition ist die Schnittmenge der definierten Teilketten und wird als solche in \autoref{lst:koop} durch ihre Wiederholung visualisiert.

\begin{lstlisting}[language=JSON,firstnumber=1, float, caption={Aufgabenbeschreibung einer zusammenhängenden Aufgabe},captionpos=t, label={lst:koop}]
Name_der_Aufgabe: Kooperation
Aufgabenbeschreibung: 
	objekt0:
		- 0.000 -0.690 0.150
		- -0.550 -0.094 0.165
		- 0.073 0.479 0.176
		- 0.073 0.479 0.176
		- -0.625 0.957 0.173
		- 0.000 1.510 0.169
\end{lstlisting}

\section{Positionsanalyse}
Der in \autoref{sec:diffVoxel} beschriebene Algorithmus zur Ermittlung valider Basispositionen erfordert die Aufgabenbeschreibungen und zusätzlich den invertierten Arbeitsraum als Parameter. Die daraus kalkulierten Resultate werden als Marker Nachrichten aus \autoref{sec:marker} publiziert und als in RViz visualisiert. Die Nachrichten enthalten für jedes Kettenglied die resultierenden Basispositionen aus \autoref{eq:28}, deren Voxelisierung aus \autoref{sec:positioning}, die Schnittmenge pro Teilkette nach \autoref{sec:diffVoxel} und deren anschließenden Projektion auf die XY-Ebene des kartesischen Koordinatensystems, was dem \autoref{sec:ground} zu entnehmen ist. \autoref{lst:Goal} definiert die finale YAML-Datei, welche bezüglich der modellierten Operationskette, jedem Primitiv der Szene ein robotische System zuordnet. \autoref{lst:Goalschnitt} zeigt die Positionierungsdatei für das Beispiel aus \autoref{lst:schnitt}. Dabei enthält das Feld \texttt{Basispositionen} die Positionen mit der größten Bewertung pro Teilkette nach der Berechnung, welche dem \autoref{sec:diffVoxel} zu entnehmen ist.

\begin{lstlisting}[language=JSON,firstnumber=1, caption={Positionsanalyse der Schnittaufgabe},captionpos=t, label={lst:Goalschnitt}]
Referenzsystem: Emika Franka 'Panda'
Name_der_Aufgabe: Schnitt
Roboter_Anzahl: 2 
Zeit: 
Move_Groups: 
	objekt0:
		panda_arm1: 
			- 0.000 -0.690 0.150
			- -0.550 -0.094 0.165
	objekt1:
		panda_arm2:
			- 0.073 0.479 0.176
			- -0.625 0.957 0.173
Basispositionen:
	panda_arm1: str % "x y z"
	panda_arm2: str % "x y z"
\end{lstlisting}

\section{Ausführung der Operationskette}
Zur Ausführung der Operationskette ist ein weiterer Algorithmus implementiert, welcher die erforderlichen Modifikationen aus \autoref{sec:Modifikationen} anhand des \texttt{Basispositionen} Feldes einer Positionierungsdatei realisiert. Somit werden die Positionen der robotischen Systeme \texttt{panda\_arm1} und \texttt{panda\_arm2} für das Beispiel in \autoref{lst:Goalschnitt} aktualisiert, die Primitive und Auflageflächen nach \autoref{sec:Konstruktion} anhand der Kettenglieder innerhalb der \texttt{panda\_arm} Felder generiert. In der entstandenen Szene erfolgt anschließend die Exekution der Aufgabe.