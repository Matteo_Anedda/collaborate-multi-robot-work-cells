\chapter{Grundlagen}\label{ch:basics}
Wesentliche Konzepte dieser Bachelorarbeit fundieren auf verschiedenen wissenschaftlichen Publikationen, welche Aspekte der Zielsetzung aufgreifen und näher erläutern. Dabei werden Begriffe mehrdeutig verwendet, weil keine offizielle Definition existiert oder sich deren Bedeutungen überschneiden. Ebenfalls basieren die Ansätze auf mathematischen Konzepten, die zwar genannt aber nicht ausreichend erläutert werden. Zum näheren und besseren Verständnis der Ideen und Algorithmen, die Bestandteil dieser wissenschaftlichen Arbeit sind, werden diese zugrundeliegenden Strukturen zusätzlich erklärt und gegebenenfalls für dieses Dokument definiert. Aus der Zielsetzung, die in \autoref{sec:Zielsetzung} formuliert ist, ergibt sich die Anforderungsanalyse als finale Komponente dieses Kapitels.


\section{Räumliche Partitionierung}
\label{sec:partitionierung}
Die räumliche Partitionierung beschreibt das Unterteilen eines kartesischen Raumes in kleinere, disjunkte Teilräume und deren Indexierung in entsprechenden Datenstrukturen. Die Implementierung einer solcher Datenstruktur ist oft eine erweiterte Baumstruktur mit speziellen Eigenschaften, die dessen Einsatz im 3 dimensionalen Raum begünstigen \cite{Hornung2013}. Beispielsweise sind solche Bäume aufgrund dessen Kompaktheit für die Aufbewahrung großer Datenmengen geeignet oder ermöglichen eine schnelle Suche unter dessen Blättern durch ihre Implementierung. In der Literatur wird die Unterteilung eines quadratischen Raumes in Volumen fester Größe \emph{Voxelisierung} genannt \cite{Makhal2018} \cite{Zacharias2007} \cite{Porges2015}. Diese Volumen beziehungsweise \emph{Voxel} definieren disjunkte Teilräume des kartesischen Raums.

\section{Diskretisierung eines Objektes}
\label{sec:diskretisierung}
Die Diskretisierung ist bekannt als ein Prozess der Zerlegung einer kontinuierliche Oberfläche durch Abtastung in ihre diskreten Teilbereiche oder Punkte, welche im Kontext dieser Arbeit durch Vektoren beschrieben sind. Anwendung findet dieses Verfahren Beispielsweise im Gebiet der Netzwerktechnologie, in der ein analoges Signal durch räumliche und zeitliche Diskretisierung in ein digitales Signal überführt wird. Angewendet auf 3-dimensionale Objekte entsteht durch Diskretisierung ein Gitter aus Vektoren, welche das zugrundeliegende Objekt approximieren \cite{Porges2015}. 

\section{Robotische Systeme}
Robotische Systeme setzen sich aus einer Menge an Festkörpern und deren Verknüpfungen durch verschiedene Gelenktypen zusammen. Ein solches System spannt eine Baumstruktur auf, dessen Wurzel durch eine fixe Basis definiert ist. Ein solcher kinematischer Baum ist verzweigt, sobald ein Festkörper Verknüpfungen zu mehr als einem weiteren Festkörper aufweist. Solche kinematischen Strukturen werden als verzweigte beziehungsweise unverzweigte kinematische Kette denotiert. Bildet der Baum keinen Kreis, so ist er eine offene kinematische Kette beziehungsweise ein offenes robotisches System, dessen letztes Glied als Endeffektor deklariert ist, während der erste Festkörper die Basis des Roboters darstellt \cite[46ff.]{Siciliano2016}. Roboterarme sind Beispiele für offene robotische Systeme beziehungsweise offene kinematischer Ketten und Bezugspunkt dieser wissenschaftlichen Arbeit. Nähere Informationen über die Kopplung spezifischer Festkörper werden im Kontext dieser Arbeit in der zugehörigen URDF \footnote{\url{http://wiki.ros.org/urdf}} beziehungsweise XACRO \footnote{\url{http://wiki.ros.org/xacro}} Datei des Roboters dokumentiert. Innerhalb dieser Dateien können zusätzliche Informationen in Form von \emph{tags}, beispielsweise Limitierungen bezüglich des Bewegungsumfangs eines Festkörpers, spezifiziert werden. \par 
Ein Roboterarm kann anhand seiner Komponenten durch das Inkludieren einzelner URDF Dateien modelliert werden. Neben der Möglichkeit, ein robotisches System eigens zu konstruieren, sind fertige Systeme öffentlich \footnote{\url{http://wiki.ros.org/urdf/Examples}} zugänglich. So ist es beispielsweise auch die Roboterbeschreibung des Referenzsystems dieser wissenschaftlichen Arbeit, des Roboterarms \emph{Panda} der Franka Emika GmbH \footnote{\url{http://wiki.ros.org/franka_ros}}.
 
\section{Kinematische Grundlagen} 
Nachdem bereits die Klassifizierung eines robotischen Systems anhand seiner Baumstruktur eruiert wurde, werden in diesem Abschnitt weitere kinematischen Aspekte näher erläutert. Grundlegende Methodiken dieser wissenschaftlichen Arbeit fundieren auf Konzepten der Kinematik, welche als Teilbereich der Physik die Bewegung von Festkörpern in robotischen Systemen, abstrahierend von den daraus resultierenden Kräften und Momenten, umfasst \cite[9f.]{Siciliano2016}. Ein wesentlicher Aspekt ist hierbei die Repräsentation der Position und Orientierung von Körpern im 3-dimensionalen Raum und deren Relation zueinander, sowie die Beschreibung der Geometrie robotischer Mechanismen. 

\subsection{Pose, Position und Orientierung} 
Die Kombination aus Position und Orientierung eines Festkörpers relativ zu einem Frame definiert eine \emph{Pose}. Frames bestehen aus dessen Koordinatenursprung und orthogonalen Basisvektoren. Beispielsweise beinhaltet das bekannte Referenzkoordinatensystem  $Frame_{0}$ den Ursprung $(0,0,0)^{T}$ und Basisvektoren $(0,0,1)^{T}, (0,1,0)^{T}, (1,0,0)^{T}$ auf deren Grundlage eine $^{0}Pose$ relativ zu $Frame_{0}$ definiert und repräsentiert werden kann. Ein robotisches System ist durch die Definition der $n \in \N$ Frames für jedes seiner Festkörper in der URDF Datei konkretisiert \cite[9ff.]{Siciliano2016}.  

\subsubsection{Position}
Der Koordinatenursprung eines Frames $i$ relativ zum Frame $j$ wird durch den Vektor $^{j}p_{i} \in \R^{3}$ gekennzeichnet. 
 
\begin{equation}  \label{eq:1}
^{j}p_{i} = \begin{pmatrix}
              ^{j}p_{i}^{x} \\
              ^{j}p_{i}^{y} \\
              ^{j}p_{i}^{z} \\
              \end{pmatrix}
\end{equation}

Aus \autoref{eq:1} ist ersichtlich, dass die Elemente des Koordinatenursprungs von Frame $i$ auf Koordinaten relativ zu $j$ referenzieren. Diese Abhängigkeit besteht analog zwischen Frames eines Roboters, welche jeweils relativ zum Vorgänger Frame der kinematischen Kette definiert sind.
 
\subsubsection{Orientierung}
Eine Orientierung wird durch Rotationen $R$ dargestellt, die auf einen Körper angewendet werden können. Im Forschungsgebiet Robotik und der Computergrafik existieren verschiedene Ansätze zur mathematischen Beschreibung einer Rotation, welche sich konzeptuell unterscheiden. Beispielsweise kann eine Rotation durch Kompositionen aus Rotations-Matrizen oder der Anwendung komplexer Zahlen realisiert werden. Die spezielle RPY-Rotation\footnote{\url{https://de.wikipedia.org/w/index.php?title=Roll-Nick-Gier-Winkel&oldid=209197743}\label{RPY}}, welche auf den Rotationswinkeln eines Körpers hinsichtlich seiner Achsen basiert, wird in \autoref{fig:1} anhand eines Flugzeugs visualisiert und ist Bestandteil späterer Ausführungen.

\begin{figure}[ht]
\includegraphics[width = 5cm, height = 5cm]{images/RPY.png}
\caption[RPY Rotation]{Roll-Pitch-Yaw beziehungsweise Roll-, Nick- und Gier- Winkel an einem Flugzeug demonstriert.\footref{RPY}}
\label{fig:1}
\end{figure}




Die Menge aller Rotations-Matrizen $R \in \R^{3 \times 3}$ bilden mit der Matrixmultiplikation die nicht kommutative Gruppe $SO(3) \subset \R^{3\times3}$ \cite[17f.]{Siciliano2016}, dessen Gruppenaxiome in \autoref{eq:SO} formuliert sind. Dies ist die spezielle orthogonale Gruppe $SO(3)$, wobei das Einselement die Matrix 1 ist, deren diagonale mit eins belegt ist, während alle anderen Werte null sind.

\begin{equation} \label{eq:SO}
\begin{split}
&Geschlossenheit:  	\forall R_{1},R_{2} \in SO(3):  R_{1}R_{2} \in SO(3) \\
&Identität:  \forall R \in SO(3): 1R = R1 = R\\
&Inverse:  \forall R \in SO(3): RR^{T}= 1  \\
&Assoziativität:  \forall R_{1},R_{2},R_{3} \in SO(3): (R_{1}R_{2})R_{3} = R_{1}(R_{2}R_{3})
\end{split}
\end{equation}

Die Komposition von Rotationen verschiedener Frames wird in \autoref{eq:2} veranschaulicht. Dabei ist die Einhaltung der Reihenfolge bezüglich der Multiplikation, aufgrund der mangelnden Kommutativität der Gruppe SO(3), erforderlich. Durch dessen Eigenschaft der Geschlossenheit, ist das Resultat dieser Multiplikation ebenfalls $\in SO(3)$ \cite[11f.]{Siciliano2016}.

\begin{equation} \label{eq:2}
^{k}R_{i} = \prescript{k}{}{R}_{j} \times \prescript{j}{}{R}_{i}
\end{equation}

\subsubsection{homogene Transformation}
Eine homogene Matrix $T \in \R^{4\times4}$, bestehend aus einer Rotationsmatrix $R \in SO(3)$ und einem Vektor $p \in \R ^{3}$, beschreibt eine Transformations-Matrix, siehe \autoref{eq:3}. Diese ermöglicht die synchrone Ausführung von Translation und Rotation, wobei eine Translation die Verschiebung eines beliebigen Vektors beschreibt, ohne sich auf dessen Orientierung auszuwirken. Die aus Frame Rotationen $^{j}R_{i}$ und Koordinatenursprung $^{j}p_{i}$ bestehende Transformations-Matrix $^{j}T_{i}$ eignet sich am besten zur Beschreibung einer Pose $i$ und ermöglichen zusätzlich die Transformation eines Vektors $^{i}u$ in den Vektor $^{j}u$, wie  \autoref{eq:4} zeigt.

\begin{equation}
\label{eq:3}
\begin{split}
T  &= \begin{pmatrix}
              R & p \\
              0 & 1 \\
      \end{pmatrix} \\
^{j}T_{i} &=  \begin{pmatrix}
							^{j}R_{i} & ^{j}p_{i} \\
              0 & 1 \\
              \end{pmatrix}
\end{split}
\end{equation}

\begin{equation}
\label{eq:4}
\begin{pmatrix}
	^{j}u \\
	1 \\
\end{pmatrix}
= \prescript{j}{}{T}_{i} \times \begin{pmatrix}
														^{i}u \\
														1 \\
													\end{pmatrix}
\end{equation}

Die Transformations-Matrix $T$ bilden mit der Matrixmultiplikation eine nicht kommutative Gruppe $SE(3)$, dessen Gruppenaxiome in \autoref{eq:SE} beschrieben sind, hierbei gilt analog zu \autoref{eq:SO} die 1 Matrix $\in \R^{4\times4}$ als das neutrale Element der Gruppe \cite[17f.]{Siciliano2016}. 

\begin{equation}\label{eq:SE}
\begin{split}
&Geschlossenheit:  	\forall T_{1},T_{2} \in SE(3):  T_{1}T_{2} \in SE(3) \\
&Identität:  \forall T \in SE(3): 1T = T1 = T\\
&Inverse:  \forall ^{j}T_{i} \in SE(3): (^{j}T_{i})(^{j}T_{i}^{-1}) = (^{j}T_{i})(^{i}T_{j}) = 1 \\
&Assoziativität:  \forall T_{1},T_{2},T_{3} \in SE(3): (T_{1}T_{2})T_{3} = T_{1}(T_{2}T_{3})
\end{split}
\end{equation}

Die Komposition aus Transformationen verschiedener Frames wird in Gleichung \autoref{fig:5} veranschaulicht. Dabei ist die Einhaltung der Reihenfolge bezüglich der Multiplikation, analog zur \autoref{eq:2} Gruppe $SO(3)$, erforderlich. Durch die Eigenschaft der Geschlossenheit der Gruppe SE(3), ist das Resultat dieser Multiplikation ebenfalls $\in SE(3)$ \cite[14f.]{Siciliano2016}.

\begin{equation}\label{eq:5}
^{k}T_{i} = \prescript{k}{}{T}_{j} \times \prescript{j}{}{T}_{i}
\end{equation}

Für die kinematische Kette eines robotischen Systems bestehenden aus $n \in \N$ Festkörpern, wobei $Frame_{n} = Frame_{Endeff}$ das Frame des Endeffektors ist, gilt \autoref{eq:6}, welche zusätzlich ein Beispiel für $n=8$ zeigt. Im Kontext dieser Arbeit bedeutet diese Gleichung, dass für jede vom Roboter erreichbare Endeffektor-Pose relativ zu $Frame_{0}$ eine solche Kette aus Transformationen existiert, welche jeweils die Pose eines Festkörpers des Roboters repräsentieren \cite[26f.]{Siciliano2016}.

\begin{equation}
\label{eq:6}
\begin{split}
^{0}T_{n} &= \prod_{i=0}^{n}{^{n-i}T_{i}} \\
^{0}T_{8} &= \prescript{0}{}{T}_{1} \times \prescript{1}{}{T}_{2} \times \prescript{2}{}{T}_{3} \times \prescript{3}{}{T}_{4} \times \prescript{4}{}{T}_{5} \times \prescript{5}{}{T}_{6} \times \prescript{6}{}{T}_{7} \times \prescript{7}{}{T}_{8}
\end{split}
\end{equation}

\begin{figure}[ht]
\includegraphics[]{images/panda_chain.png}
\caption[Kinematische Kette des Franka Panda Roboters]{Visualisierung der kinematischen Kette eines Franka Emika 'Panda' Roboters anhand der Frames aller Festkörper.}
\label{fig:PC}
\end{figure}


  

\section{Arbeitsbereich eines robotischen Systems}
Der Arbeitsbereich \emph{Workspace}, beschreibt die Kapazitäten eins robotische Systems bezüglich der Interaktion mit dessen Umfeld. \autoref{fig:panda} aus dem Manual \cite{frankaHB} des Referenzroboters 'Panda' zeigt dessen Arbeitsbereich schematisch aus verschiedenen Perspektiven. Dabei erfolgt keine detaillierte Charakterisierung eines Bereiches, denn Eigenkollisionen des Roboters werden beispielsweise nicht erfasst. In dieser wissenschaftlichen Arbeit ist daher die Ermittlung spezieller Datenstrukturen erforderlich, um den Arbeitsbereich des Referenzroboters differenziert zu inspizieren. Zacharias et al. \cite{Zacharias2007} \cite{Zacharias2009} definieren hierzu \emph{reachability maps} und \emph{capability maps}, welche die Erreichbarkeit der Vektoren $v$ im Umfeld des Roboters durch den Endeffektor klassifizieren.

\ffigbox[\FBwidth]%
  {\begin{subfloatrow}%
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.47\textwidth, height = 5.5cm]{images/panda_sv.png}}}%
      {\caption{Seitenansicht}\label{fig:SV}}%
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.47\textwidth, height =5.5cm]{images/panda_tv.png}}}%
      {\caption{Ansicht von oben}\label{fig:TV}}%
  \end{subfloatrow}}%
  {\caption[Franka Panda Roboter aus mehreren Perspektiven]{Illustration des Arbeitsraums eines Panda Roboters der Franka Emika GmbH aus unterschiedlichen Perspektiven. Alle Angaben sind in Millimeter bemessen.}\label{fig:panda}}%

\subsection{reachability map}
Die reachability map ist eine Datenstruktur, welche die Erreichbarkeit eines Vektors $v$ anhand aller Orientierungen charakterisiert, die der Endeffektor einnehmen kann. Sie enthält eine Menge aus Tupeln, welche die Posen des Endeffektors sowie der Information bezüglich deren Erreichbarkeit in Form eines Wahrheitswertes spezifiziert. Diese Informationen können zusätzlich durch die Integration einer Metrik genutzt werden, um beispielsweise den Arbeitsbereich 3 dimensional darzustellen, oder mit den Posen weitere Berechnungen durchzuführen \cite{Zacharias2009}. Aufgrund des Aufbaus einer reachability map, eignet sich diese als Grundlage für spätere Verfahren und wird in dieser wissenschaftlichen Arbeit genutzt.

\subsection{capability map}
Capability maps sind Erweiterungen der reachability map, welche aufbauend auf den Inhalt einer reachability map weitere Metriken implementieren. Die Approximation der erfolgreich terminierten kinematischen Kalkulationen für einen Vektor $v$ durch Primitive, beispielsweise Kegeln oder Zylindern, ist der grundlegende Ansatz dieser Datenstruktur. Demnach kann die Erreichbarkeit eines Vektors darin gemessen werden, welches Primitiv seine erfolgreichen Orientierungen approximiert. Diese Informationen können analog zur reachability map visualisiert und persistiert werden \cite{Zacharias2007}. 

\section{Das ROS Framework und MoveIt}
ROS \cite{Quigley} ist ein auf robotische Systeme spezialisiertes open-source Framework, mit dem Ziel, Implementierungen bezüglich robotischer Systeme zu vereinheitlichen. Exekutierende Prozesse im ROS Kontext sind \emph{Nodes}. Diese Kommunizieren über spezifische Nachrichten, welche beispielsweise Listen, primitive Datentypen oder weitere Nachrichten enthalten können mit anderen Nodes. Dies erfolgt über das \emph{publisher-subscriber} Prinzip, wobei Nachrichten bezüglich eines Themas (\emph{Topic}) publiziert werden, welche von anderen Nodes durch das Abonnieren des Themas empfangen werden. ROS Pakete (\emph{packages}) realisieren kooperatives Arbeiten, indem die Nodes größerer Projekte koordiniert werden können, so kann ein Paket beispielsweise die graphische Visualisierung eines Prozesses beinhalten, während ein anderes Paket andere Aspekte des Projekts, wie die Ausführung von Algorithmen. Grundsätzlich ist ein ROS package ein Verzeichnis, in dem eine XML-Datei die Abhängigkeiten zwischen enthaltenen Nodes und genutzten Bibliotheken dokumentiert. 

\subsection{Visualisierung in ROS}
Während die textuelle Visualisierung primitiver Datentypen oder des Inhalts spezifischer Nachrichten über die Kommandozeile möglich sind, ermöglicht das \emph{publisher-subscriber} Prinzip die Implementierung separater Prozesse, welche Informationen anhand des Abonnierens einer topic visuell repräsentieren. Ein Beispiel hierfür ist \emph{RViz} als fester Bestandteil von ROS. RViz ist durch die Integration eigener Module erweiterbar und ermöglicht die 3 dimensionale Visualisierung von Frames, robotischer Systeme oder weiteren Objekten in Form von \emph{Meshes} \cite{Quigley}. \autoref{fig:RViz} zeigt eine leere Szene in RViz und dessen grafischer Benutzeroberfläche, wobei das dargestellte Gitters auf der horizontalen XY Ebene, dessen Kanten jeweils einen Meter voneinander distanziert sind, zur Orientierung innerhalb der Szene dient. 

\begin{figure}[ht]
\includegraphics[width = 8cm, height = 7cm]{images/RViz.png}
\caption{Leere RViz Szene}
\label{fig:RViz}
\end{figure}

\subsubsection{Marker Elemente}
\label{sec:marker}
Primitive wie Würfel, Pfeile oder Sphären sind Inhalt simple Marker Nachrichten, welche anhand deren Position, Orientierung, Farbe und Dimension definiert sind und in RViz durch das Abhören des zugrundeliegenden Themas visualisiert werden, um die Resultate der zugrundeliegenden Implementierung zu inspizieren beziehungsweise zu analysieren.

\subsubsection{Interaktive Marker Elemente}
Interaktive Marker erweitern die bereits präzisierten Marker um die Interaktionsmöglichkeit durch den Nutzer, wodurch Translationen und Rotationen auf beliebigen Achsen des Markers durch den Mauszeiger realisierbar sind. Zusätzlich kann jeder Marker ein eigenes Menü mit Untermenüs initialisieren, durch dessen Einträge zusätzliche anwendungsspezifische Algorithmen implementiert werden können. Diese Modifikationen werden durch zugrundeliegende visuelle Kontrollelemente des Markers und deren Kommunikation mit dem \emph{Interactive Marker Server} \cite{DavidGossow2011} realisiert. Die Visualisierung dieser Marker mit ihren Kontrollelementen in RViz zeigt \autoref{fig:IM1}, wobei die Translation und Rotation durch Interaktion mit den Pfeilen beziehungsweise Kreisbögen innerhalb der grafischen Benutzeroberfläche von RViz erfolgt. \autoref{fig:IM2} zeigt die Kommunikation zwischen dem RViz Prozess und dem interactive Marker Server. Dabei wird jede Interaktion mit einer Instanz des interaktiven Markers von RViz publiziert. Der Server reagiert auf diese Nachricht mit der aktualisierten Position oder Orientierung des Markers. Zusätzlich ist die Implementierung benutzerdefinierter Funktionen möglich, die als Reaktion auf eine Handlung in RViz vom Server ausgeführt werden sollen.

\begin{figure}
\ffigbox[\FBwidth]%
  {\begin{subfloatrow}%
    \ffigbox[\FBwidth]%
      {\fbox{
					\begin{tikzpicture} [ >=stealth']
					\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (IMS) {Interactive Marker Server};
					\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (R) [below = 3.7cm]{RViz};
					\path (IMS.south) -- (IMS.south east) coordinate[pos=0.092] (a1);
					\path (IMS.south) -- (IMS.south west) coordinate[pos=0.092] (a2);
					\path (R.north) -- (R.north east) coordinate[pos=0.4] (b1);
					\path (R.north) -- (R.north west) coordinate[pos=0.4] (b2);
					\draw[->] (a1) -- (b1);
					\draw[<-] (a2) -- (b2);
					\end{tikzpicture}\label{fig:IM1}
			}}%
      {\caption{Kommunikation zwischen Server und RViz}}
    \ffigbox[\FBwidth]%
      {\fbox{\includegraphics[width=0.47\textwidth, height =5.5cm]{images/IM2.png}}}%
      {\caption{Interaktiver Marker mit Kontrollelementen}}
  \end{subfloatrow}}%
  {\caption[Interaktive Marker und deren Server]{Darstellung eines interaktiven Markers mit visuellen Kontrollelementen und dessen Kommunikation mit der Server Instanz.}\label{fig:IM}}%
\end{figure}

\subsection{Kinematische Operationen in ROS}
\label{sec:Moveit}
In dieser wissenschaftlichen Arbeit erfolgen Berechnungen bezüglich der Kinematik, Kollision und Bewegungsplanung ausschließlich mit der open source Software \emph{MoveIt!} \cite{Chitta2016}. Diese definiert den \emph{move\_group} Prozess als Zentrum der modularen Architektur, welcher durch Integration zusätzlicher Prozesse beziehungsweise deren \emph{topics} und Nachrichten beispielsweise die Exekution kinematischer Operationen erlaubt. Dieser zentrale Prozess wird in einem Konfigurationspaket initialisiert, welches zuvor innerhalb des \emph{MoveIt! Setup Assistant (MSA)} konstruiert wird. Dieser spezifiziert beispielsweise die SRDF \footnote{\url{http://wiki.ros.org/srdf}} des zugrundeliegenden robotischen Systems und enthält weitere Konfigurationen zu dessen Visualisierung und Simulation. 

\section{Anforderungen automatischer kollaborativer Multi-Roboter Arbeitsräume}
\label{sec:anforderungen}
Marvel et al. \cite{Marvel2018} adressieren die Skalierbarkeit, Synchronisierung der Roboter, Verteilung der spezifischen Aufgaben und Kollisionsvermeidung als grundlegende Aspekte der Planung eines Multi-Roboter Systems. Diese Aspekte stehen in Wechselwirkung zueinander, denn die Kollisionsdetektion ist beispielsweise vereinfacht, wenn die Exekution der Aufgaben sequentiell erfolgt und daher nur ein Roboter zeitgleich Bewegungen ausführt, dies impliziert zugleich eine weniger komplexe Aufgabenverteilung. Erfolgen die Handlungen der Roboter synchron, so ist die Verteilung der der Aufgaben essenziell für eine erfolgreiche Terminierung und dessen Ausführungszeit und bedingt eine komplexere Kollisionsdetektion. Änderungen der Szenerie, Beispielsweise durch das Platzieren oder Entfernen eines Roboters, haben ebenfalls Auswirkungen auf die Ausführung beziehungsweise Kalkulationszeit, welche die Skalierbarkeit des Arbeitsraums konkretisiert. Diese Aspekte sind zusätzlich abhängig von deren Position in der Domäne, welche die Generierung automatischer Multi-Roboter Arbeitsräume ermöglicht und daher der Hauptaspekt dieser wissenschaftlichen Arbeit ist. Also eigentlich kann ich hier alles aus Evaluation rein kopieren, das klingt zumindest besser.
