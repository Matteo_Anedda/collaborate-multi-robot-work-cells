\chapter{Implementierung}\label{ch:implementation}
Die ausführliche Auseinandersetzung mit der Struktur des ROS spezifischen Arbeitsraums, dessen Pakete die Roboterbeschreibungen, Konfigurationsdateien und implementierte Algorithmen enthalten, ist Inhalt dieses Kapitels und in \autoref{fig:8} illustriert. Dabei initialisiert jede Quelldatei einen Node im ROS Kontext in der Programmiersprache C++. 

\begin{figure}[h!]
\centering
	\begin{tikzpicture} [ >=stealth']
		\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (RD) {Roboterbeschreibung};
		\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (DK) [above right = 1cm and 0.5cm of RD] {Dual Konfiguration};
		\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (MK) [below right = 1cm and 0.5cm of RD]{Single Konfiguration};
		\node[rectangle, draw=black,  thick, minimum height=3em, align=center] (I) [right= 5cm of RD]{Implementierung};
		
		\node[rectangle, fill= white, draw=black,  thick, minimum height=1.5em, align=center] (qe) at (RD.south east)  {URDF};
		\node[rectangle, fill= white, draw=black,  thick, minimum height=1.5em, align=center] (adsew) at (DK.south east)  {SRDF};
		\node[rectangle, fill= white, draw=black,  thick, minimum height=1.5em, align=center] (wyxve) at (MK.south east)  {SRDF};

		\draw[-] (RD) -- (DK) -- (I);
		\draw[-] (RD) -- (MK) -- (I);



	\end{tikzpicture}
	\caption[ROS Pakete dieser Arbeit]{Die Struktur des ROS Arbeitsraums bestehend aus der Roboterbeschreibung mit URDF-Dateien und den MoveIt! Konfigurationspaketen mit SRDF-Dateien} \label{fig:8}
\end{figure}

\section{Robotische Systeme und deren Konfiguration}
Die Roboterbeschreibung ist Grundlage der Generierung valider MoveIt! Konfigurationspakete, welche jeweils $n \in \N_{>0}$ robotische Systeme konkretisieren und somit die Kommunikation zwischen den $n$ Robotern durch Integration der \emph{move\_group} Schnittstelle ermöglichen. Beispielsweise ist der Roboterbeschreibung aus \autoref{fig:9} links zu entnehmen, dass ein Manipulator aus Hand und Arm besteht, sowie dass die Instanziierung mehrerer Manipulatoren eine zusätzliche Datei benötigen. \autoref{fig:9} rechts zeigt das Konfigurationspaket, welches im \texttt{config} Verzeichnis die SRDF-Datei des robotischen Systems enthält. \autoref{lst:4} zeigt am Beispiel einer URDF-Datei für 2 robotische Systeme, welche Modifikationen aus \autoref{sec:Modifikationen} notwendig sind, um die Position des Roboters aus der Positionsdatei zu laden, dessen Struktur \autoref{lst:Goal} zu entnehmen ist.

\begin{figure}
\centering
\begin{minipage}{.45\linewidth}
\dirtree{%
.1 Roboterbeschreibung.
.2 ci.
.2 meshes.
.2 robots.
.3 'panda\_arm.xacro'.
.3 'hand.xacro'.
.3 'panda\_arm\_hand.urdf.xacro'.
.3 'dual\_panda\_example.urdf.xacro'.
}
\end{minipage}\hfill
\begin{minipage}{.55\linewidth}
\dirtree{%
.1 MoveIt\_Konfiguration.
.2 config.
.3 'panda.srdf'.
.2 launch.
}
\end{minipage}\hfill
\caption{Visualisierung der Verzeichnisstruktur von Roboterbeschreibung und Konfigurationspaketen}\label{fig:9}
\end{figure}

\begin{lstlisting}[language=JSON,firstnumber=1, float, caption={Modifikationen in der URDF-Datei des robotischen Systems }, label={lst:4}, captionpos=t]
<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="panda">
<!-- Einbindung der Roboter Komponenten-->
<xacro:include filename="panda_arm.xacro"/>
<xacro:include filename="hand.xacro"/>

<!-- Deklaration der Name der Positionsdatei -->
<xacro:arg name="GOAL" default='Base_task'/>

<xacro:property name="yaml_file" value="$(find reachability)/Positionsanalyse/$(arg GOAL).yaml" />
<xacro:property name="props" value="${load_yaml(yaml_file)}" />

<!-- Laden der Positionen aus den Feldern der Datei-->
<xacro:property name="pos_1" value="${props['Basispositionen']['panda_arm1']}" />
<xacro:property name="pos_2" value="${props['Basispositionen']['panda_arm2']}" />

<!-- Definition des eindeutigen World-Frames, als Referenz der Roboter -->
<link name="world"/>

<!-- Initialisierung der robotischen Systeme an ihrer spezifischen Position-->
<xacro:panda_arm arm_id="$(arg arm_id_1)" connected_to="world"  xyz="${pos_1}" /> 
<xacro:hand ns="$(arg arm_id_1)" rpy="0 0 ${-pi/4}" connected_to="$(arg arm_id_1)_link8" />

<xacro:panda_arm arm_id="$(arg arm_id_2)" connected_to="world"  xyz="${pos_2}" /> 
<xacro:hand ns="$(arg arm_id_2)" rpy="0 0 ${-pi/4}" connected_to="$(arg arm_id_2)_link8"/>
</robot>$
\end{lstlisting}


\subsection{Move\_Group Schnittstelle für Multi-Roboter Arbeitsräume}
Die SRDF-Datei des Konfigurationspakets konkretisiert ein robotisches System als Bewegungsgruppe \emph{Move\_Group}, deren Identität eindeutig durch ihr Namensattribut bestimmt ist. Demnach genügt dieses Attribut zur Instanziierung eines \emph{move\_group} Objekts, welches die Kommunikation zu dem spezifischen Roboter initiiert. Eine duales Konfigurationspaket ermöglicht analog das Instanziieren und Ansteuern zweier robotischer Systeme durch deren Namensattribute. Dabei ist eine Fehlfunktion dieser Instanzen während der Interaktion mit dem Primitiv einer Aufgabe observierbar. Der Versuch das Primitiv zu greifen, wird als Kollision detektiert, woraufhin die auszuführende Handlung fehlschlägt. Dies impliziert, dass die Funktionen hinsichtlich der Betrachtung mehrerer robotischer Systeme limitiert sind. Der \autoref{lst:5} zeigt einen Ansatz, welcher die Kollisionsdetektion definierter Kollisionsobjekte in der \emph{Allowed Kollision Matrix} (AKM) deaktiviert. Diese Modifikation ermöglicht das Greifen, Halten und Abstellen eines Primitives. 


\begin{lstlisting}[language=AST,firstnumber=1, caption={Modifikation der Kollisionsmatrix}, label={lst:5}, float, captionpos=t]
/*
Des Endeffektors rechter und linker Finger kollidieren unweigerlich während der Exekution mit dem Primitiv, wodurch beispielsweise die Greifoperation fehlschlägt.
*/

// Initialisierung der Szene
planning_scene::PlanningScene ps(kinematic_model);

// Generierung der Bewegungsgruppen anhand der festgelegten Namensattribute innerhalb der SRDF
std::vector<moveit::planning_interface::MoveGroupInterface>  robots;
for(int i = 1; i <= 2; i++)
	robots.push_back(std::move(moveit::planning_interface::MoveGroupInterface("panda_arm"+ std::to_string(i))))
	
// Modifikation der Kollisionsmatrix, welche die erlaubten Kollisionen der robotischen Systeme anhand deren Adjazenzen enthält 
collision_detection::AllowedCollisionMatrix acm = ps.getAllowedCollisionMatrix();

for(int i = 1; i <= 2; i++){
	acm.setEntry("object" + std::to_string(i), "panda_" + std::to_string(i) + "_leftfinger", true);
	acm.setEntry("object" + std::to_string(i), "panda_" + std::to_string(i) + "_rightfinger", true);
}

// Publikation der Aenderungen als Inhalt einer Planning Scene Nachricht
moveit_msgs::PlanningScene pls;
acm.getMessage(pls.allowed_collision_matrix);
pls.is_diff = true;
planning_scene_diff_publisher.publish(pls);
\end{lstlisting}

\subsection{Implementierung der Arbeitsraumanalyse}
\autoref{sec:Aanal} ist zu entnehmen, dass während der Arbeitsraumanalyse die Menge aller Positionen $P_{total}$ sowie deren Orientastionen $OR_{total}$ generiert werden, um diese als Grundlage der kinematischen Berechnungen zu nutzen. Dazu sind neben der kubischen Diskretisierung zusätzliche sphärische Diskretisierungen formuliert, unter anderem der von Deserno \autoref{Deserno2015} illustrierte Algorithmus zur gleichmäßigen sphärischen Diskretisierung. Eine Implementierung für performante Rechner ist wird durch Hilfsfunktionen implementiert, welche die Menge aller Endeffektor-Posen $T$ in $n \in \N>0$ Teilmengen spaltet. Diese disjunkten Teilmengen können parallel von $n$Thread bearbeitet werden.

\subsection{Implementierung des Inversen Arbeitsraums}
Der Algorithmus des inversen Arbeitsraums kalkuliert anhand persistierter Informationen der Arbeitsraumanalyse die Bewertung einer Position, welche in \autoref{eq:18} dokumentiert ist. Die Inversion aller Transformationen anhand einer Iteration über die \emph{reachability map}, erfolgt durch die in der Bibliothek \emph{tf2}\footnote{\url{http://wiki.ros.org/tf2}} implementierte Realisierung homogener Transformationen und deren Operationen. Zusätzlich erfolgt die Portierung der Metrik und Persistenz der kalkulierten inverse reachability map.

\section{Der Algorithmus zur Positionsanalyse}
Die Implementierung der Positionsanalyse erfolgt über deren Voxelisierung durch Integration der PCL Bibliothek \footnote{\url{http://wiki.ros.org/pcl_ros}}. Diese ist in C++ vollumfänglich implementiert und ermöglicht performante Operationen auf 3-dimensionale Koordinaten. Wie \autoref{sec:diffVoxel} zu entnehmen ist, erfolgt zunächst eine Voxelisierung nach \autoref{sec:partitionierung} für jedes Kettenglied einer Teilkette durch das \texttt{Octree} Objekt der PCL Bibliothek. Alle kalkulierten Basispositionen $P_{Base}$ nach \autoref{eq:28} des betrachteten Kettenglied werden in dieses Objekt integriert, wodurch jede Position $p_{Base} \in P_{Base}$ einem Voxel zugeordnet wird. Anschließend erfolgt eine Abtastung anhand der \texttt{VoxelSearch()} Funktion, welche die enthaltenen Basispositionen $p_{Base}$ eines Voxels übergibt, sofern er abgetastet wird. \par
Die Abtastung erfolgt anhand einer kubisch Diskretisierten Struktur, welche bezüglich des \texttt{Octree} Objektes eine identische Auflösung hat. Im Anschluss erfolgt eine Filter-Funktion auf dies Struktur, welche die Schnittmenge der Voxel pro Teilkette errechnet.


Eine Teilkette $TK \subset K$ definiert nun anhand ihrer Glieder $g_{i}$ die Einträge, welche der Vektor $^{0}v$ mindestens enthalten muss, um eine potentielle Position des robotischen Systems zu sein, welche $TK$ operiert. Durch die Abbildung $D_{port}({0}T_{Base}) = D_{Reach}({0}T_{Base})$ ist analog die Struktur $Task_{Metrik}$ implementiert, welche die portierten Metriken aller Transformation $^{0}T_{Base_{i}}$ auf den Vektor $^{0}v$ abbildet.

\begin{equation}
\begin{split}
\label{eq:31}
Task_{Metrik} &= {(i, D_{port}(^{0}T_{Base_{i}})) | g_{i} \in K} \\
Dictionary_{Metrik} &= {(^{0}v, Task_{Metrik})}
\end{split}
\end{equation}

Die Ermittlung der optimalen Position $^{0}v$ für eine Teilkette $TK$ erfolgt, indem das arithmetische Mittel pro Kettenglied  $TK \in g_{i}$ kalkuliert wird und in Verhältnis zur Kardinalität $\vert TK \vert$ gesetzt wird. \par 
Die resultierende Position pro $TK$ wird in einem eigenen Verzeichnis persistiert, um als Parameter der \emph{Pick and Place} Implementierung geladen zu werden und über jede Instanz, von der Konfigurationsdatei in die Roboterbeschreibung übergeben zu werden.  

