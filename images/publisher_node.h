#ifndef PUBLISHER_NODE_H
#define PUBLISHER_NODE_H

#include "ros/ros.h"
#include <ros/package.h>

#include "visualization_msgs/InteractiveMarker.h"
#include "geometry_msgs/Vector3.h"
#include "moveit/move_group_interface/move_group_interface.h"
#include "math.h"

#include <nlohmann/json.hpp>
#include <fstream>
#include <regex>
typedef std::vector<std::pair<tf2::Vector3,std::vector<std::pair<tf2::Quaternion, bool>>>> dictionary;

void cube_dec(const float& abstand, const float& resolution, dictionary& dict);
std::vector<tf2::Quaternion> sphere_dec(const float& radius, const int& count, const tf2::Vector3& origin);
tf2::Quaternion quat_builder(const float& phi, const float& nu);


float lerp (float a, float b, float f);
std_msgs::ColorRGBA col_lerp (const float& f);
#endif

